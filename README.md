# Wifiteca
<img src="https://pbs.twimg.com/media/C77S-pnW0AAxIcL.jpg">

Te damos la bienvenida a este proyecto educativo.  
Queremos realizar un proyecto colaborativo para proveer de una red wifi local con contenido para el aprendizaje.  
Nuestra idea es crear una biblioteca digital inalámbrica, uniendo Cultura Maker, Software Libre y contenido Creative Commons.  
Es una Fiburcación del proyecto [Library Box](http://www.librarybox.us/).

Si quieres colaborar con el proyecto, ponte en contacto de las siguientes formas:  
Blog: <https://wifiteca.gitlab.io>  
Correo: <wifiteca@gmail.com>  
Twitter: <https://twitter.com/wifiteca>  
Gitlab: <https://gitlab.com/wifiteca>  
Youtube: <https://www.youtube.com/wifiteca>  